const LoremIpsum = require("lorem-ipsum").loremIpsum;
const express = require('express');
const router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', {title: 'Express'});
});

router.get('/questions', function (req, res, next) {
    res.send({
        questions: Array(5).fill().map(function (v, i) {
            return ({
                question: LoremIpsum(1) + "?",
                answers: Array(4).fill().map(function (v, n) {
                    return {
                        text: LoremIpsum(1),
                        valid: n === ((i + 2) % 4)
                    };
                })
            });
        })
    })
});

module.exports = router;
